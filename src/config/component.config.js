import PageHeader from '@/components/Layout/PageHeader'
import SearchBar from '@/components/SearchBar'
import { BaseTable } from '@/components/BaseTable.js'

export const assignComponents = Vue => {
  Vue.component('PageHeader', PageHeader)
  Vue.component('SearchBar', SearchBar)
  Vue.component('BaseTable', BaseTable)
}
