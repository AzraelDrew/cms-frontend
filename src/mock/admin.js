import Mock, { Random } from 'mockjs'
import qs from 'qs'

const courseList = Mock.mock({
  'data|88': [
    {
      'id|+1': 1,
      courseName: () => Random.cword(4, 10),
      teacherName: () => Random.name(),
      courseIntro: () => Random.cparagraph(1, 3)
    }
  ]
})

Mock.mock(/api\/admin\/courseList/, 'get', options => {
  const params = qs.parse(options.url.split('?')[1])
  const page = Number(params.page)

  const result = {
    success: true,
    data: {
      total: courseList.data.length,
      page,
      list: courseList.data.slice((page - 1) * 10, page * 10)
    }
  }

  return result
})

Mock.mock('/api/admin/delCourse', 'post', () => {
  return {
    success: true,
    msg: ''
  }
})

Mock.mock('/api/admin/addCourse', 'post', () => {
  return {
    success: true,
    msg: ''
  }
})

Mock.mock('/api/admin/editCourse', 'post', () => {
  return {
    success: true,
    msg: ''
  }
})

const teacherList = Mock.mock({
  'data|88': [
    {
      'id|+1': 1,
      name: () => Random.name(),
      'sex|1-2': 1,
      phone: /^1\d{10}$/,
      rank: '教师',
      number: () => Random.string()
    }
  ]
})

Mock.mock(/api\/admin\/teacherList/, 'get', options => {
  const params = qs.parse(options.url.split('?')[1])
  const page = Number(params.page)

  if (page) {
    return {
      success: true,
      total: teacherList.data.length,
      page,
      data: teacherList.data.slice((page - 1) * 10, page * 10)
    }
  }

  return {
    success: true,
    data: teacherList.data
  }
})

Mock.mock('/api/admin/delTeacher', 'post', () => {
  return {
    success: true,
    msg: ''
  }
})

Mock.mock('/api/admin/addTeacher', 'post', () => {
  return {
    success: true,
    msg: ''
  }
})

Mock.mock('/api/admin/editTeacher', 'post', () => {
  return {
    success: true,
    msg: ''
  }
})

const studentList = Mock.mock({
  'data|88': [
    {
      'id|+1': 1,
      name: () => Random.name(),
      'sex|1-2': 1,
      phone: /^1\d{10}$/,
      number: () => Random.string()
    }
  ]
})

Mock.mock(/api\/admin\/studentList/, 'get', options => {
  const params = qs.parse(options.url.split('?')[1])
  const page = Number(params.page)

  return {
    success: true,
    total: studentList.data.length,
    page,
    data: studentList.data.slice((page - 1) * 10, page * 10)
  }
})

Mock.mock('/api/admin/delStudent', 'post', () => {
  return {
    success: true,
    msg: ''
  }
})

Mock.mock('/api/admin/addStudent', 'post', () => {
  return {
    success: true,
    msg: ''
  }
})

Mock.mock('/api/admin/editStudent', 'post', () => {
  return {
    success: true,
    msg: ''
  }
})
