import Mock, { Random } from 'mockjs'
import qs from 'qs'

const courseList = Mock.mock({
  'data|100': [
    {
      'id|+1': 1,
      courseName: () => Random.cword(4, 10),
      teacherName: () => Random.name(),
      courseIntro: () => Random.cparagraph(1, 3),
      'isSelected|0-1': 1
    }
  ]
})

Mock.mock(/api\/student\/courseList/, 'get', options => {
  const params = qs.parse(options.url.split('?')[1])
  const page = Number(params.page)

  const result = {
    success: true,
    data: {
      total: courseList.data.length,
      page,
      list: courseList.data.slice((page - 1) * 10, page * 10)
    }
  }

  return result
})

Mock.mock('/api/student/changeCourseStatus', 'post', options => {
  const params = JSON.parse(options.body)
  const id = Number(params.id)
  const action = Number(params.action)

  courseList.data.find(item => item.id === id).isSelected = action

  return {
    success: true,
    msg: ''
  }
})

const scoreList = Mock.mock({
  'data|17': [
    {
      'id|+1': 1,
      courseName: () => Random.cword(4, 10),
      teacherName: () => Random.name(),
      'score|0-100': 1
    }
  ]
})

Mock.mock(/api\/student\/scoreList/, 'get', options => {
  const params = qs.parse(options.url.split('?')[1])
  const page = Number(params.page)

  const result = {
    success: true,
    data: {
      total: scoreList.data.length,
      page,
      list: scoreList.data.slice((page - 1) * 10, page * 10)
    }
  }

  return result
})
