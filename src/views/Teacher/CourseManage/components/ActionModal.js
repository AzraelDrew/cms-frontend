import { Modal, Form, Input } from 'ant-design-vue'

const scoreValidator = (rule, value, callback) => {
  const reg = /(^[1-9][0-9]$|^[0-9]$|^100$)/
  if (!reg.test(Number(value))) {
    callback(new Error('请输入正确的分数'))
  }
  callback()
}

export const ActionModal = Form.create({})({
  props: {
    visible: Boolean,
    loading: Boolean,
    dataSource: {
      type: Object,
      default: () => ({})
    }
  },

  methods: {
    handleCancel() {
      this.form.resetFields()
      this.$emit('close')
    },

    handleSubmit() {
      const {
        form,
        dataSource: { id }
      } = this
      form.validateFields((errors, values) => {
        if (!errors) {
          const params = {
            ...values,
            id
          }

          this.$emit('submit', { form, params })
        }
      })
    }
  },

  render() {
    const {
      visible,
      loading,
      dataSource: { courseName, studentName, message, score },
      form: { getFieldDecorator }
    } = this

    return (
      <Modal
        visible={visible}
        title="评分"
        onCancel={this.handleCancel}
        onOk={this.handleSubmit}
        confirmLoading={loading}
      >
        <Form labelCol={{ span: 8 }} wrapperCol={{ span: 12 }}>
          <Form.Item label="课程名">{courseName}</Form.Item>

          <Form.Item label="学生姓名">{studentName}</Form.Item>

          <Form.Item label="分数">
            {getFieldDecorator('score', {
              initialValue: score ? score.toString() : '',
              rules: [
                { required: true, mesasge: '请输入分数' },
                { validator: scoreValidator }
              ]
            })(<Input />)}
          </Form.Item>

          <Form.Item label="评价">
            {getFieldDecorator('message', {
              initialValue: message,
              rules: [{ max: 50, message: '字数不得超过50字' }]
            })(<Input.TextArea />)}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
})
