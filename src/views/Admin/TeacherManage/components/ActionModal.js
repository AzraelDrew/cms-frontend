import { Modal, Form, Input, Select } from 'ant-design-vue'
import { phoneValidator } from '@/utils/validator'

export const ActionModal = Form.create({})({
  props: {
    visible: Boolean,
    loading: Boolean,
    dataSource: {
      type: Object,
      default: () => ({})
    }
  },

  methods: {
    handleCancel() {
      this.form.resetFields()
      this.$emit('close')
    },

    handleSubmit() {
      const { form, dataSource } = this
      form.validateFields((errors, values) => {
        if (!errors) {
          const data = {
            ...dataSource,
            ...values
          }

          this.$emit('submit', { form, data })
        }
      })
    }
  },

  render() {
    const {
      visible,
      loading,
      dataSource: { name, sex, number, phone, id, rank },
      form: { getFieldDecorator }
    } = this

    return (
      <Modal
        visible={visible}
        title="教师"
        onCancel={this.handleCancel}
        onOk={this.handleSubmit}
        confirmLoading={loading}
      >
        <Form labelCol={{ span: 8 }} wrapperCol={{ span: 12 }}>
          <Form.Item label="工号">
            {getFieldDecorator('number', {
              initialValue: number || '',
              rules: [{ required: true, message: '请输入工号' }]
            })(<Input disabled={Boolean(id)} />)}
          </Form.Item>

          <Form.Item label="姓名">
            {getFieldDecorator('name', {
              initialValue: name || '',
              rules: [{ required: true, message: '请输入姓名' }]
            })(<Input />)}
          </Form.Item>

          <Form.Item label="性别">
            {getFieldDecorator('sex', {
              initialValue: sex,
              rules: [{ required: true, message: '请选择性别' }]
            })(
              <Select>
                <SelectOption value={1}>男</SelectOption>
                <SelectOption value={2}>女</SelectOption>
              </Select>
            )}
          </Form.Item>

          <Form.Item label="职称">
            {getFieldDecorator('rank', {
              initialValue: rank || '',
              rules: [{ required: true, message: '请输入职称' }]
            })(<Input />)}
          </Form.Item>

          <Form.Item label="联系电话">
            {getFieldDecorator('phone', {
              initialValue: phone || '',
              rules: [
                { required: true, message: '请输入联系电话' },
                { validator: phoneValidator }
              ]
            })(<Input />)}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
})
