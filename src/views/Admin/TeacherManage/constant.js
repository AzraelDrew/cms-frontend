const SEX_MALE = 1
const SEX_FEMALE = 2

const sexMap = {
  [SEX_MALE]: '男',
  [SEX_FEMALE]: '女'
}

export const searchLayout = [
  [
    {
      type: 'input',
      placeholder: '姓名',
      field: 'name'
    },
    {
      type: 'input',
      placeholder: '工号',
      field: 'number'
    }
  ]
]

export const columns = [
  {
    dataIndex: 'name',
    title: '姓名'
  },
  {
    dataIndex: 'sexText',
    title: '性别'
  },
  {
    dataIndex: 'rank',
    title: '职称'
  },
  {
    dataIndex: 'phone',
    title: '联系电话'
  },
  {
    dataIndex: 'number',
    title: '工号'
  },
  {
    dataIndex: 'action',
    title: '操作',
    scopedSlots: { customRender: 'action' }
  }
]

export const dataHelper = data => {
  return data.map(item => {
    item.sexText = sexMap[item.sex]
    return item
  })
}
