export const searchLayout = [
  [
    {
      type: 'select',
      placeholder: '选课状态',
      field: 'isSelected',
      options: [
        { label: '全部', value: '' },
        { label: '已选', value: 1 },
        { label: '未选', value: 0 }
      ]
    },
    {
      type: 'input',
      placeholder: '课程名',
      field: 'courseName'
    },
    {
      type: 'input',
      placeholder: '课程ID',
      field: 'courseID'
    }
  ]
]

export const columns = [
  {
    dataIndex: 'id',
    title: '课程ID'
  },
  {
    dataIndex: 'courseName',
    title: '课程名'
  },
  {
    dataIndex: 'teacherName',
    title: '主讲老师'
  },
  {
    dataIndex: 'isSelectedText',
    title: '是否已选'
  },
  {
    dataIndex: 'action',
    title: '操作',
    scopedSlots: { customRender: 'action' }
  }
]

export const dataHelper = data => {
  return data.map(item => {
    item.isSelectedText = item.isSelected ? '是' : '否'
    return item
  })
}
