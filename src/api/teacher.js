import { request } from './request'

// 获取课程列表
export const apiGetCourseList = data => {
  return request('GET', '/api/teacher/courseList', data)
}

// 获取记录列表
export const apiGetRecordList = data => {
  return request('GET', '/api/teacher/recordList', data)
}

// 提交评分
export const apiSubmitScore = data => {
  return request('POST', '/api/teacher/editScore', data)
}
