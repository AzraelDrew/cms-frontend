import { request } from './request'

// 获取个人信息
export const apiGetPersonalInfo = () => {
  return request('GET', '/api/user/personalInfo')
}

// 修改个人信息
export const apiEditPersonalInfo = data => {
  return request('POST', '/api/user/editPersonalInfo', data)
}

// 登录
export const apiLogin = data => {
  return request('POST', '/api/user/login', data)
}

// 退出登录
export const apiLogout = () => {
  return request('POST', '/api/user/logout')
}

// 修改密码
export const apiResetPwd = data => {
  return request('POST', '/api/user/resetPwd', data)
}

// 获取导航
export const apiGetMenu = () => {
  return request('GET', '/api/user/getMenu')
}
