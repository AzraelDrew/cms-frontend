import { request } from './request'

// 获取学生课表
export const apiGetCourseList = data => {
  return request('GET', '/api/student/courseList', data)
}

// 选课/取消选课
export const apiChangeCourseStatus = data => {
  return request('POST', '/api/student/changeCourseStatus', data)
}

// 获取成绩表
export const apiGetScoreList = data => {
  return request('GET', '/api/student/scoreList', data)
}
