import Vue from 'vue'
import VueRouter from 'vue-router'
import { Storage } from '@/utils/storage'
import student from './student'
import teacher from './teacher'
import admin from './admin'

// 避免重复点击同一路由报错
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location, onResolve, onReject) {
  if (onResolve || onReject)
    return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(VueRouter)

const routes = [
  {
    path: '/account',
    component: () =>
      import(
        /* webpackChunkName:"account"*/ '../components/Layout/LoginLayout.vue'
      ),
    redirect: '/account/login',

    children: [
      {
        path: 'login',
        component: () =>
          import(
            /* webpackChunkName:"account"*/ '../views/User/Login/Login.vue'
          ),
        meta: {
          requireAuth: false
        }
      },
      {
        path: 'resetPwd',
        component: () =>
          import(
            /* webpackChunkName:"account"*/ '../views/User/ResetPwd/ResetPwd.vue'
          ),
        meta: {
          requireAuth: false
        }
      }
    ]
  },

  {
    path: '/',
    component: () =>
      import(
        /* webpackChunkName:"home"*/ '../components/Layout/GlobalLayout.vue'
      ),
    redirect: '/home',

    children: [
      {
        path: 'home',
        component: () =>
          import(/* webpackChunkName:"home"*/ '../views/Home.vue'),
        meta: {
          requireAuth: true
        }
      },

      {
        path: '403',
        component: () =>
          import(/* webpackChunkName:"home"*/ '../views/Common/403.vue'),
        meta: {
          requireAuth: true
        }
      },

      ...student,
      ...teacher,
      ...admin,

      {
        path: 'user/personalInfo',
        component: () =>
          import(
            /* webpackChunkName:"user"*/ '../views/User/PersonalInfo/PersonalInfo.vue'
          ),
        meta: {
          requireAuth: true
        }
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const userInfo = Storage.get('_user')

  // 页面权限 登录页和修改密码页不需要登录态
  const {
    meta: { requireAuth }
  } = to

  if (requireAuth) {
    if (!userInfo) {
      Vue.prototype.$message.error('请先登录')
      next('/account/login')
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
