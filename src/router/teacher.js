export default [
  {
    path: 'teacher/course',
    component: () =>
      import(
        /* webpackChunkName:"teacher"*/ '../views/Teacher/CourseManage/CourseManage.vue'
      ),
    meta: {
      requireAuth: true,
      permission: ['teacher']
    }
  }
]
